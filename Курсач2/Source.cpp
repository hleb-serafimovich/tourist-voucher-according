
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <conio.h>
#include <set>
#include <algorithm>
#include <limits> 
#include <windows.h>
#include <cstdio>  



#define DEBUG

using namespace std;

#if defined(max)
#undef max
#endif

string INFO_FILE = "C:/dd/Info.txt"; //���� � �����
const string USERS_FILE = "C:/dd/Users.txt";
const int ADMIN = 1;
const int USER = 0;

struct Info           //  C�������� �������
{
	string typeOfTour;   // ��� �������
	string country;     // ������
	string transport;  // ��� �����������
	string food;      // ��� �������
	int daysAmount;  // ���-�� ����
	double value;   // ���� �������

};
struct User
{
	string login;
	string password;
	int role;
};


vector<Info> infoVector;
vector<User> userVector;
set<string> countrys;
set<string> kindsOfTransport;
set<string> kindOfTour;
set<string> kindOfFood;


int accountNamber;





//~~~~~~~~~~~~~~~~~~~~~  ����  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~




int AdminMenu();			       // ���� ��������������							
int UserMenu();                   // ���� ������������
void DataMenu();                 // ���� ��� ������ � �������
void proccesMenu();			    // ���� ��������� ������
void dataSearchMenu();         //  ���� ������ ������
void dataSortingMenu();       // ���� ��������� ������
void EditMenu();             // ���� �������������� ������
void AccountsMenu();        // ���� ��� ������ � �������� ��������
void FileMenu();           // ���� ��� ������ � �������
int backMunu();           // ��������� � ���������� ����






 //~~~~~~~~~~~~~~~~~~~~~~~~ ���� �����, ���������� � ������� ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

string getCountrys();     // ���� ������
string getTransport();   // ���� ����������
string getTour();       // ���� ���� �������
string getFood();      // ���� ���� �������



//~~~~~~~~~~~~~~~~~~~~~  ������� ��� �������� ������ ~~~~~~~~~~~~~~~~~~~~~~~~~




void CheckInfoFile();       // �������� ���������� �� ���� � �������
void CheckUsersFile();     // ������ ���������� �� ���� � �������� �������� 





   //~~~~~~~~~~~~~~~~~~~~~  ������� ��� ������ � �����   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

void addInfoinArray();                            // ���������� ����� �����
void showInfoArray();                            // ����� ������ �� �����
void updateInfoArray();                         // �������������� ������
void deleteInfoArray();                        // �������� ������
void addUserinArray();                        // ���������� ������� ������
void showUserArray();                        // ����� ������� ������� �� �����
void updateUserArray();                     // �������������� ������� ������
void deleteUserArray();                    // �������� ������� ������



   //~~~~~~~~~~~~~~~~~~~~~~~~~~~   VALIDATION  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



double getValue(int from, int to); // ���� �����
bool isNumber();
bool isNumberRange(double value, int from, int to);

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ �����  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

void searchByCountry();
void searchByTypeOfTour();
void searchByTypeOfTransport();


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ���������� ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

void sortByPrice();
void sortByQuantityOfDay();
void sortByCountryAlphabet();
bool isNumberGreater(Info first, Info second);
bool isQuantityOfDayGreater(Info first, Info second);
bool isletterGreater(Info first, Info second);



//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ������� ��� ������ � �������  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

void writeFileUsers();                                // ������ � ���� ������
void readFileUsers();                                // ������ �� ����� ������
void writeFileInfo();                               // ������ � ���� ������
void readFileInfo(string fileName);                // ������ �� ����� � ������ ������
void CheckInfoFile();
void CheckUsersFile();
void createFile();
void deleteFile();
void openFile();




//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~   ������� ������ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


void AddAdminEntry();






//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~   �����������   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

int Authorisation();
void ChangeUser(int &role);
bool isLoginUnique(string login);


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~      ������ �������        ~~~~~~~~~~~~~~~~~~~~~~~~~
 
void ProgramRealization(int user_role);
void individualTask();
void createSets();       // �������� ��������� ��������� 
void CompletedAction(); // ��������� ����� � ���������� �������� 
int ConfirmAction();   // �������� ������������� �������� 





void main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	bool exit_flag = true;
	int user_role;

	CheckUsersFile();
	CheckInfoFile();
	createSets();
	readFileUsers();
	readFileInfo(INFO_FILE);
	user_role =  Authorisation();
	ProgramRealization(user_role);
	writeFileInfo();
	writeFileUsers();
	

}




//~~~~~~~~~~~~~~~~~~~~~~~ VALIDATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


bool isNumber()
{
	if (cin.get() == '\n')
		return true;
	else {
		
		cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n');
		return false;
	}
}

bool isNumberRange(double value, int from, int to)
{
	if (value >= (double)from && value <= (double)to)
		return true;
	else
		return false;
}





double getValue(int from, int to)
{
	double value;
	while (true)
	{
	
		cin >> value;
		if (isNumber() && isNumberRange(value, from, to))
		{  //cin >> number ���������� true ���� ������� ��������
			return value;
		}
		else
		{
			cout << "\n\t\t������������ ����!" << endl;
			cout << "\t\t��������� ����: ";
		}
	}
}










//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~    ������� ��� ������ � �������    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


void CheckUsersFile()
{
	ifstream read(USERS_FILE, ios::in);
	if (!read.is_open())
	{
		read.close();
		ofstream create(USERS_FILE, ios::out);
		create.close();
		AddAdminEntry(); // �������� ������ �������������� � ������ ����
		cout << "\t������ ���� : " << USERS_FILE << endl;
		system("pause");
	}


	read.close();

}

void AddAdminEntry()
{
	User admin;
	admin.login = "admin";
	admin.password = "admin1";
	admin.role = ADMIN;

	ofstream fadd(USERS_FILE, ios::app);
	fadd << admin.login << " "
		<< admin.password << " "
		<< admin.role << " ";
	fadd.close();

}


void CheckInfoFile()
{
	ifstream read(INFO_FILE, ios::in);
	if (!read.is_open())
	{
		read.close();
		ofstream create(INFO_FILE, ios::out);
		create.close();
		cout << "\n\t\t������ ����: " << INFO_FILE << endl;
		system("pause");

	}

	read.close();

}



void readFileUsers()
{
	
	ifstream read(USERS_FILE, ios::in);
	User temp;
	if (!read.is_open())
	{
		cout << "\n\t\t��������� ���� �� ����������!" << endl;
		system("pause");
	}
	
	else
	{
		int i = 0;
		while (!read.eof())
		{
			read 
			    >> temp.login
				>> temp.password
				>> temp.role;
			if (temp.login == "")
			{
				cout << "\n\t\t���� " << USERS_FILE << " ����!\n";
				break;
			}
			userVector.push_back(temp);
			i++;
		}

	}


	read.close();

}


void writeFileUsers()
{
	
	ofstream write(USERS_FILE, ios::out);  //������� ���� ��� �����
	for (int i = 0; i < userVector.size(); i++)
	{
		write
			<< userVector[i].login << "\t"
			<< userVector[i].password << "\t"
			<< userVector[i].role;
		if (i < userVector.size() - 1)
		{
			write << endl;
		}
	}
	write.close();
}





void readFileInfo(string fileName)
{
	
	Info temp;
	ifstream read(fileName, ios::in);
	if (!read.is_open())
	{
		cout << "\n\t\t��������� ���� �� ����������!" << endl;
	}

	else
	{
		int i = 0;
		while (!read.eof())
		{
			read 
				>> temp.typeOfTour
				>> temp.country
				>> temp.transport
				>> temp.daysAmount
				>> temp.food
				>> temp.value;
				if (temp.typeOfTour == "")
				{
					cout << "\n\t\t���� " << fileName << " ����!\n";
					break;
			    }

			infoVector.push_back(temp);
			i++;
	
		}


	}
	

	read.close();

}

void writeFileInfo()
{

	int choice;
	bool flag_exit = true;
	system("cls");
	cout << "\n\n\n\n\n\n\n";
	cout << "\t\t\t\t _________________________________________________________________" << endl;
	cout << "\t\t\t\t|                                                                 |" << endl;
	cout << "\t\t\t\t|\t      � � � � � � � � �   � � � � � � � � � ?  \t\t  | " << endl;
	cout << "\t\t\t\t|                                                                 |" << endl;
	cout << "\t\t\t\t|_________________________________________________________________|" << endl;
	cout << "\t\t\t\t|                                |                                |" << endl;
	cout << "\t\t\t\t|                                |                                |" << endl;
	cout << "\t\t\t\t|     ����������� - ENTER        |         ��������� - ESC        |" << endl;
	cout << "\t\t\t\t|                                |                                |" << endl;
	cout << "\t\t\t\t|________________________________|________________________________|" << endl;

	while (flag_exit)
	{
		choice = _getch();
		if (choice == 13)
		{
			system("cls");
			ofstream write(INFO_FILE, ios::out);  //������� ���� ��� �����
			for (int i = 0; i < infoVector.size(); i++)
			{
				write
					<< infoVector[i].typeOfTour << "\t"
					<< infoVector[i].country << "\t"
					<< infoVector[i].transport << "\t"
					<< infoVector[i].daysAmount << "\t"
					<< infoVector[i].food << "\t"
					<< infoVector[i].value;
				if (i < infoVector.size() - 1)
				{
					write << endl;
				}
			}
			CompletedAction();
			write.close();
			flag_exit = false;
		}

		if (choice == 27)
		{
			exit(1);
		}
	}
		
}

void createFile()
{
	if (backMunu() == 0)
	{
		return;
	}

	system("cls");
	cout << "  \n\t\t ________________________________________________________________________________________________________" << endl;
	cout << "  \t\t| \t\t\t\t       � � � � � � � �   � � � � �" << "\t\t\t\t\t |" << endl;
	cout << "  \t\t|________________________________________________________________________________________________________|" << endl;
	string name;
	cout << "\n\t\t������� ������ ��� �����: ";
	cin >> name;
	ifstream read(name, ios::in);
	if (!read.is_open()) {
		read.close();
		ofstream create(name, ios::out);
		CompletedAction();
		create.close();
	}
	else {
		cout << "\t\t���� � ������: " << name <<  " ��� ����������\n\n " << endl;
		system("pause");
		read.close();
	}
	
}

void deleteFile()
{
	if (backMunu() == 0)
	{
		return;
	}
	system("cls");
	cout << "  \n\t\t ________________________________________________________________________________________________________" << endl;
	cout << "  \t\t| \t\t\t\t       � � � � � � � �   � � � � �" << "\t\t\t\t\t |" << endl;
	cout << "  \t\t|________________________________________________________________________________________________________|" << endl;
	char name[80];
	int choice;
	bool flag_exit = true;
	cout << "\n\t\t������� ������ ��� �����: ";
	cin >> name;

	if (name == USERS_FILE)
	{
		cout << "\n\t\t���� ���� ������� ������!\n\n";
		system("pause");
		return;

	}

	while (flag_exit)
	{
		choice = ConfirmAction();
		switch (choice)
		{
		case 13:
			if (remove(name) != 0)// �������� ����� file.txt
			{
				cout << "\n\t\t\t\t������ �������� �����n\n\n";
				system("pause");
			}
			     
			else
				CompletedAction();
			    flag_exit = false;
			break;
		case 27:
			flag_exit = false; 
			break;
		}
	}

}

 

void openFile()
{
	if (backMunu() == 0)
	{
		return;
	}

	string fileName;
	int choice;
	bool flag_exit = true;
	system("cls");
	cout << "  \n\t\t ________________________________________________________________________________________________________" << endl;
	cout << "  \t\t| \t\t\t\t        � � � � � � � �   � � � � �" << "\t\t\t\t\t |" << endl;
	cout << "  \t\t|________________________________________________________________________________________________________|" << endl;

	cout << "\n\t\t������� ������ ��� �����: ";
	cin >> fileName;
	if (fileName == USERS_FILE)
	{
		cout << "\n\t\t���� ���� ������� ������!\n\n";
		system("pause");
		return;

	}
	ifstream read(fileName, ios::in);
	if (!read.is_open())
	{
		cout << "\t\t���� ���� �� ����������!\n\n" << endl;
		system("pause");
		return;
	}
	else
	{
	
		while (flag_exit)
		{
			choice = ConfirmAction();
			switch (choice)
			{
			case 13:
				INFO_FILE = fileName;
				infoVector.clear();
				readFileInfo(fileName);
				CompletedAction();
				flag_exit = false;
				break;
			case 27:
				flag_exit = false;
				break;
			}
		}
	}




}




//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ������� ��� ������ � ����� ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

void addInfoinArray()
{
	if (backMunu() == 0)
	{
		return;
	}

	system("cls");
	cout << "  \n\t\t ________________________________________________________________________________________________________" << endl;
	cout << "  \t\t| \t\t\t\t       � � � � � � � � � �   � � � � � �" << "\t\t\t\t |" << endl;
	cout << "  \t\t|________________________________________________________________________________________________________|" << endl;
	Info temp;
	int maxPrice = 1000000;
	int maxDayAmount = 360;

	cout << "\n\t\t������� ��� �������: ";
	temp.typeOfTour = getTour();
	cout << "\n\t\t������� ������: ";
	temp.country = getCountrys();
	cout << "\n\t\t������� ��� ����������: ";
	temp.transport = getTransport();
	cout << "\n\t\t������� ���������� ����: ";
	temp.daysAmount = getValue(0, maxDayAmount);
	cout << "\n\t\t������� ��� �������(��������/��� ��������): ";
	temp.food = getFood();
	cout << "\n\t\t������� ���������: ";
	temp.value = getValue(0, maxPrice);

	infoVector.push_back(temp);
	CompletedAction();


}

void deleteInfoArray()
{
	if (backMunu() == 0)
	{
		return;
	}

	system("cls");
	showInfoArray();

	cout << "  \n\t\t ________________________________________________________________________________________________________" << endl;
	cout << "  \t\t| \t\t\t\t       � � � � � � � �   � � � � � �" << "\t\t\t\t\t |" << endl;
	cout << "  \t\t|________________________________________________________________________________________________________|" << endl;

	int delete_index;
	int choice;
	bool flag_exit = true;
	if (infoVector.size() == 0)
	{
		cout << "\n\t\t C ������ �������� ������!\n\n";
		system("pause");
		return;

	}
	cout << "\n\t\t ������� ����� ������: ";
	delete_index = getValue(1, infoVector.size());
	while (flag_exit)
	{
		choice = ConfirmAction();
		switch (choice)
		{
		case 13: 
			infoVector.erase(infoVector.begin() + (delete_index - 1));
			CompletedAction();
			flag_exit = false;
			break;
		case 27:
			flag_exit = false;
			break;
		}

	}
}


void showInfoArray()
{
	system("cls");
	
		
		cout << "\n\t _______________________________________________________________________________________________________________________" << endl;
		cout << "\t| � |\t��� ����   | " << "\t   ������       | " << "    ��� �����������    | " << "   ���-�� ����    | " << "  ��� �������  |" << "       ����      | " << endl;
		cout << "\t|___|______________|____________________|________________________|___________________|________________|_________________|" << endl;
		for (int i = 0; i < infoVector.size(); i++)
		{
			cout << "\t| "<< i + 1  << " |\t";
			cout << setw(11) << left;
			cout << infoVector[i].typeOfTour << "|\t   ";
			cout << setw(10) << left;
			cout << infoVector[i].country << "\t|\t  ";
			cout << setw(13) << left;
			cout << infoVector[i].transport << "\t |\t  ";
			cout << setw(6) << left;
			cout << infoVector[i].daysAmount << "     |\t ";
			cout << setw(8) << left;
			cout << infoVector[i].food << "     |\t      ";
			cout << setw(8) << left;
			cout << infoVector[i].value << "  |"<<endl;
			cout << setw(8) << left
			<< "\t|___|______________|____________________|________________________|___________________|________________|_________________|\n";

			
		}
	
	
	system("pause");
}


void updateInfoArray()
{
	system("cls");

	Info temp;
	int  choise;

	

	int  maxDay = 360, maxPrice = 1000000;

	showInfoArray();

	cout << "  \n\t\t ________________________________________________________________________________________________________" << endl;
	cout << "  \t\t| \t\t\t       � � � � � � � � � � � � � �   � � � � � �" << "\t\t\t\t |" << endl;
	cout << "  \t\t|________________________________________________________________________________________________________|" << endl;
	if (infoVector.size() == 0)
	{
		cout << "\n\t\t C ������ �������� ������!\n\n";
		system("pause");
		return;

	}
	cout << "\n\t\t������� ����� ������: ";
	choise = getValue(1, infoVector.size());
	cout << "\n\t\t������� ��� �������: ";
	temp.typeOfTour = getTour();
	cout << "\n\t\t������� ������: ";
	temp.country = getCountrys();
	cout << "\n\t\t������� ��� ����������: ";
	temp.transport = getTransport();
	cout << "\n\t\t������� ���������� ����: ";
	temp.daysAmount = getValue(0, maxDay);
	cout << "\n\t\t������� ��� �������(��������/��� ��������): ";
	temp.food = getFood();
	cout << "\n\t\t������� ���������: ";
	temp.value = getValue(0, maxPrice);

	infoVector[choise - 1] = temp;

	CompletedAction();

}




//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~




void addUserinArray()
{
	

	if (backMunu() == 0)
	{
		return;
	}

	system("cls");
	User temp;
	int choice;
	cout << "  \n\t\t ________________________________________________________________________________________________________" << endl;
	cout << "  \t\t| \t\t\t        � � � � � � � � � �   � � � � � � �   � � � � � �" << "\t\t\t |" << endl;
	cout << "  \t\t|________________________________________________________________________________________________________|" << endl;
	if (userVector.size() == 0)
	{
		cout << "\n\t\t C������ �������� ������!\n\n";
		system("pause");
		return;

	}
	while (true) 
	{
		cout << "\n\t\t������� ����� : ";
		cin >> temp.login;
		if (isLoginUnique(temp.login))
			break;
		cout << "\n\t\t���� ����� ��� ����������!\n\n";
		cout << "\t\tESCAPE - �����\n ";
		cout << "\t\tSPACE  - ���������\n\n";
		choice = _getch();
		if (choice == 27)
			return;
		if (choice == ' ')
			continue;
	}

	cout << "\t\t������� ������: ";
	cin >> temp.password;

	cout << "\t\t������� ����: ";
	temp.role = getValue(0, 1);
	userVector.push_back(temp);
	CompletedAction();
	
}

void deleteUserArray()
{
	

	if (backMunu() == 0)
	{
		return;
	}

	int choice;
	bool flag_exit = true;

	showUserArray();
	cout << "  \n\t\t ________________________________________________________________________________________________________" << endl;
	cout << "  \t\t| \t\t\t        � � � � � � � �   � � � � � � �   � � � � � �" << "\t\t\t\t |" << endl;
	cout << "  \t\t|________________________________________________________________________________________________________|" << endl;



	if (userVector.size() == 0)
	{
		cout << "\n\t\t C ������ �������� ������!\n\n";
		system("pause");
		return;

	}

	while (flag_exit)
	{
		cout << "\n\t\t������� ����� ������� ������: ";
		int delete_index = getValue(1, userVector.size());
		if (--delete_index == accountNamber)
		{
			cout <<"\n\t\t ������ ������� ��� ������!\n\n";
			system("pause");
			return;
		}
		choice = ConfirmAction();
		switch (choice)
		{
		case 13:
			userVector.erase(userVector.begin() + (delete_index - 1));
			CompletedAction();
			flag_exit = false;
			break;
		case 27:
			flag_exit = false;
			break;
		}

	}

}


void showUserArray()
{
	system("cls");

	cout << endl << endl;
	cout << "  \t\t ________________________________________________________________________________________________________" << endl;
	cout << "  \t\t| \t\t\t        � � � � � � �   � � � � � � �   � � � � � � �" << "\t\t\t\t |" << endl;
	cout << "  \t\t|________________________________________________________________________________________________________|" << endl;
	cout << "\t\t ________________________________________________________________________________________________________" << endl;
	cout << "\t\t| � |\t\t�����\t\t |\t\t������\t\t  |\t     ����(1-admin, 0-user)\t |" << endl;
	cout << "\t\t|___|____________________________|________________________________|______________________________________|" << endl;
	for (int i = 0; i < userVector.size(); i++)
	{
		cout << "\t\t| "<< i + 1 << " |\t\t";
		cout << setw(12) << left;
		cout << userVector[i].login << "\t |\t\t";
		cout << setw(10) << left;
		cout << userVector[i].password << "\t  |\t";
		cout << setw(15) << left;
		cout << " " << userVector[i].role << " \t\t |" << endl;
		cout << "\t\t|___|____________________________|________________________________|______________________________________|" << endl;
	}
		
	cout << endl << endl << endl;
	system("pause");
}


void updateUserArray()
{
	

	User temp;
	int  choice;

	if (backMunu() == 0)
	{
		return;
	}

	showUserArray();


	cout << "  \n\t\t _________________________________________________________________________________________________________" << endl;
	cout << "  \t\t| \t\t\t  � � � � � � � � � � � � � �   � � � � � � �   � � � � � �" << "\t\t\t  |" << endl;
	cout << "  \t\t|_________________________________________________________________________________________________________|" << endl;

	if (userVector.size() == 0)
	{
		cout << "\n\t\t C ������ �������� ������!\n\n";
		system("pause");
		return;

	}
	

	cout << "\n\t\t������� ����� ������: ";
	choice = getValue(1, userVector.size());
	while (true)
	{
		cout << "\t\t������� ����� : ";
		cin >> temp.login;
		if (isLoginUnique(temp.login))
			break;
		cout << "\n\t\t���� ����� ��� ����������!\n\n";
		cout << "\t\tESCAPE - �����\n ";
		cout << "\t\tSPACE  - ���������\n\n";
		int item = _getch();
		if (item == 27)
			return;
		if (item == ' ')
			continue;
	}

	cout << "\t\t������� ������: ";
	cin >> temp.password;

	cout << "\t\t������� ����: ";
	temp.role = getValue(0, 1);

	userVector[choice - 1] = temp;

	CompletedAction();



}

int backMunu()
{
	int item;
	while (true)
	{
		system("cls");
		cout << "\t _________________________________" << endl;
		cout << "\t|  ESC   |" << "        � � � � � \t  |" << endl;
		cout << "\t|________|________________________|" << endl;
		cout << "\t _________________________________" << endl;
		cout << "\t|  SPACE |" << "   � � � � � � � � � �  |" << endl;
		cout << "\t|________|________________________|" << endl;

		item = _getch();
		if (item == 27)
			return 0 ;
		if (item == ' ')
			break;
	}
}







//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  ���� �����, ������� � ���������� ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


void createSets()
{
	countrys.insert("������");
	countrys.insert("�����������");
	countrys.insert("�������");
	countrys.insert("�����");
	countrys.insert("���������");
	countrys.insert("��������");
	countrys.insert("�������");
	countrys.insert("��������");
	countrys.insert("��������");
	countrys.insert("�������");
	countrys.insert("��������");
	countrys.insert("������");
	countrys.insert("������");
	countrys.insert("�����");
	countrys.insert("���");
	countrys.insert("������");
	countrys.insert("������");
	countrys.insert("������");
	countrys.insert("������");
	countrys.insert("������");
	countrys.insert("�������");
	countrys.insert("������");
	countrys.insert("�������");
	countrys.insert("����");
	countrys.insert("��������");
	countrys.insert("��������");
	countrys.insert("������");
	countrys.insert("���������");
	countrys.insert("�������");
	countrys.insert("������");
	countrys.insert("����������");
	countrys.insert("���-������");
	countrys.insert("��������");
	countrys.insert("�������");
	countrys.insert("���");
	countrys.insert("������");
	countrys.insert("������");
	countrys.insert("�����");
	countrys.insert("�������");
	countrys.insert("�����");
	countrys.insert("���������");
	countrys.insert("��������");
	countrys.insert("�����");
	countrys.insert("�����");
	countrys.insert("��������");
	countrys.insert("�����-����");
	countrys.insert("����");
	countrys.insert("�������");
	countrys.insert("������");
	countrys.insert("����������");
	countrys.insert("��������");
	countrys.insert("�������");
	countrys.insert("����");
	countrys.insert("������");
	countrys.insert("���������");
	countrys.insert("���������");
	countrys.insert("�������");
	countrys.insert("���");
	countrys.insert("�����");
	countrys.insert("������");

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ���������� ����������

	kindsOfTransport.insert("�������");
	kindsOfTransport.insert("�������");
	kindsOfTransport.insert("�������");
	kindsOfTransport.insert("�����");

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  ��� �������

	kindOfTour.insert("�����");
	kindOfTour.insert("���������");
	kindOfTour.insert("�������");
	kindOfTour.insert("������");
	kindOfTour.insert("�����");

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ��� �������

	kindOfFood.insert("��������");
	kindOfFood.insert("���");





}

string getCountrys()
{
	string tempCounty;

	while (true)
	{
		cin >> tempCounty;
		if (countrys.find(tempCounty) != countrys.end())
		{
			break;
		}
		cout << "\n\t\t��� ����� ������!" << endl;
		cout << "\t\t��������� ����: ";
	}
	return tempCounty;

}

string getTransport()
{
	string tempTransport;
	while (true)
	{
		cin >> tempTransport;
		if (kindsOfTransport.find(tempTransport) != kindsOfTransport.end())
		{
			break;
		}
		cout << "\n\t\t��� ������ ����������!" << endl;
		cout << "\t\t��������� ����: ";
	}
	return tempTransport;

}


string getTour()
{
	string tempTour;
	while (true)
	{
		cin >> tempTour;
		if (kindOfTour.find(tempTour) != kindOfTour.end())
		{
			break;
		}
		cout << "\n\t\t��� ����� �������!" << endl;
		cout << "\t\t��������� ����: ";
	}
	return tempTour;

}


string getFood()
{
	string tempFood;
	while (true)
	{
		cin >> tempFood;
		if (kindOfFood.find(tempFood) != kindOfFood.end())
		{
			break;
		}
		cout << "\n\t\t��� ������ ���� �������!" << endl;
		cout << "\t\t��������� ����: ";
	}

	return tempFood;

}


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ����������� ~~~~~~~~~~~~~~~~~~~~~~~

int Authorisation()
{
	system("cls");
	string login;
	string password;
	bool flag_exit = true;
	int index_of_entry = -1;
	int chouse;
	cout << "  \n\t\t ________________________________________________________________________________________________________" << endl;
	cout << "  \t\t| \t\t\t\t       � � � � � � � � � � �" << "\t\t\t\t\t\t |" << endl;
	cout << "  \t\t|________________________________________________________________________________________________________|" << endl;
	while (flag_exit)
	{
		cout << "\n\t\t ������� �����: ";
		cin >> login;
		for (int i = 0; i < userVector.size(); i++)
		{
			if (userVector[i].login == login)
			{
				index_of_entry = i;
				accountNamber = i;
				flag_exit = false;
				break;
			}
			
		}

		if (index_of_entry < 0)
		{
			
			cout << "\t\t ����������� ������ �����!\n\n ";
			cout << "ESCAPE - �����\n ";
			cout << "SPACE  - ���������\n";
			while (true)
			{
				chouse = _getch();
				if (chouse == 27)
					exit(1); // ����� �� ���������
				if (chouse == ' ')
					break;
			}
		}
	}
		
		while (true) 
		{
			cout << "\n\t\t ������� ������: ";
			cin >> password;
			if (password == userVector[index_of_entry].password)
				break;

			cout << "\t\t ����������� ������ ������!\n\n ";
			cout << "ESCAPE - �����\n ";
			cout << "SPACE  - ���������\n\n";

			while (true)
			{
				chouse = _getch();
				if (chouse == 27)
					exit(1); // ����� �� ���������
				if (chouse == ' ')
					break;
			}
		}
		if (userVector[index_of_entry].role == ADMIN)
			return ADMIN;
		if (userVector[index_of_entry].role == USER)
			return USER;
	}


void ChangeUser(int &role)
{
	if (backMunu() == 0)
	{
		return;
	}

	role = Authorisation();
	
}



bool isLoginUnique(string login)
{

	for (int i = 0; i <  userVector.size(); i++) {
		if (userVector[i].login == login)
			return false;
	}
	return true;
}







//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ �����

void searchByCountry()
{
	if (backMunu() == 0)
	{
		return;
	}

	system("cls");
	cout << "\t\t __________________________________________________________________________________________________________" << endl;
	cout << "\t\t|" << "\t\t\t\t       � � � � �   � �   � � � � � �" << "\t\t\t\t\t   |" << endl;
	cout << "\t\t|__________________________________________________________________________________________________________|" << endl;
	string tempCountry;
	cout << "\n\t\t������� ������: ";
	tempCountry = getCountrys();

	cout << "\n\t _______________________________________________________________________________________________________________________" << endl;
	cout << "\t| � |\t��� ����   | " << "\t   ������       | " << "    ��� �����������    | " << "   ���-�� ����    | " << "  ��� �������  |" << "       ����      | " << endl;
	cout << "\t|___|______________|____________________|________________________|___________________|________________|_________________|" << endl;
	for (int i = 0; i < infoVector.size(); i++)
	{
		if (tempCountry == infoVector[i].country)
		{
			cout << "\t| " << i + 1 << " |\t";
			cout << setw(11) << left;
			cout << infoVector[i].typeOfTour << "|\t   ";
			cout << setw(10) << left;
			cout << infoVector[i].country << "\t|\t  ";
			cout << setw(13) << left;
			cout << infoVector[i].transport << "\t |\t  ";
			cout << setw(6) << left;
			cout << infoVector[i].daysAmount << "     |\t ";
			cout << setw(8) << left;
			cout << infoVector[i].food << "     |\t      ";
			cout << setw(8) << left;
			cout << infoVector[i].value << "  |" << endl;
			cout << setw(8) << left
				<< "\t|___|______________|____________________|________________________|___________________|________________|_________________|\n";
		}

	}
	system("pause");

}



void searchByTypeOfTour()
{
	if (backMunu() == 0)
	{
		return;
	}

	system("cls");
	cout << "\t\t __________________________________________________________________________________________________________" << endl;
	cout << "\t\t|" << "\t\t\t\t� � � � �   � �   � � � �   � � � � � � �" << "\t\t\t\t   |" << endl;
	cout << "\t\t|__________________________________________________________________________________________________________|" << endl;
	string tempTour;
	cout << "\n\t\t������� ��� �������: ";
	tempTour = getTour();

	cout << "\n\t _______________________________________________________________________________________________________________________" << endl;
	cout << "\t| � |\t��� ����   | " << "\t   ������       | " << "    ��� �����������    | " << "   ���-�� ����    | " << "  ��� �������  |" << "       ����      | " << endl;
	cout << "\t|___|______________|____________________|________________________|___________________|________________|_________________|" << endl;
	for (int i = 0; i < infoVector.size(); i++)
	{
		if (tempTour == infoVector[i].typeOfTour)
		{
			cout << "\t| " << i + 1 << " |\t";
			cout << setw(11) << left;
			cout << infoVector[i].typeOfTour << "|\t   ";
			cout << setw(10) << left;
			cout << infoVector[i].country << "\t|\t  ";
			cout << setw(13) << left;
			cout << infoVector[i].transport << "\t |\t  ";
			cout << setw(6) << left;
			cout << infoVector[i].daysAmount << "     |\t ";
			cout << setw(8) << left;
			cout << infoVector[i].food << "     |\t      ";
			cout << setw(8) << left;
			cout << infoVector[i].value << "  |" << endl;
			cout << setw(8) << left
				<< "\t|___|______________|____________________|________________________|___________________|________________|_________________|\n";
		}

	}
	system("pause");

}


void searchByTypeOfTransport()
{
	if (backMunu() == 0)
	{
		return;
	}

	system("cls");
	cout << "\t\t __________________________________________________________________________________________________________" << endl;
	cout << "\t\t|"  << "\t\t\t\t� � � � �   � �   � � � �    � � � � � � � � � �" <<"\t\t\t   |" << endl;
	cout << "\t\t|__________________________________________________________________________________________________________|" << endl;
	cout << endl << endl;
	string tempTransopt;
	cout << "\n\t\t������� ��� ����������: ";
	tempTransopt = getTransport();

	cout << "\n\t _______________________________________________________________________________________________________________________" << endl;
	cout << "\t| � |\t��� ����   | " << "\t   ������       | " << "    ��� �����������    | " << "   ���-�� ����    | " << "  ��� �������  |" << "       ����      | " << endl;
	cout << "\t|___|______________|____________________|________________________|___________________|________________|_________________|" << endl;
	for (int i = 0; i < infoVector.size(); i++)
	{
		if (tempTransopt == infoVector[i].transport)
		{
			cout << "\t| " << i + 1 << " |\t";
			cout << setw(11) << left;
			cout << infoVector[i].typeOfTour << "|\t   ";
			cout << setw(10) << left;
			cout << infoVector[i].country << "\t|\t  ";
			cout << setw(13) << left;
			cout << infoVector[i].transport << "\t |\t  ";
			cout << setw(6) << left;
			cout << infoVector[i].daysAmount << "     |\t ";
			cout << setw(8) << left;
			cout << infoVector[i].food << "     |\t      ";
			cout << setw(8) << left;
			cout << infoVector[i].value << "  |" << endl;
			cout << setw(8) << left
				<< "\t|___|______________|____________________|________________________|___________________|________________|_________________|\n";
		}

	}
	system("pause");

}





//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ����������

void sortByPrice()
{
	if (backMunu() == 0)
	{
		return;
	}

	


	sort(infoVector.begin(), infoVector.end(), isNumberGreater);
	showInfoArray();
	

}

bool  isNumberGreater(Info first, Info second)
{
	return first.value > second.value;

}




void sortByQuantityOfDay()
{
	if (backMunu() == 0)
	{
		return;
	}

	system("cls");
	sort(infoVector.begin(), infoVector.end(), isQuantityOfDayGreater);
	showInfoArray();
	
}

bool isQuantityOfDayGreater(Info first, Info second)
{
	return first.daysAmount > second.daysAmount;

}

void sortByCountryAlphabet()
{
	if (backMunu() == 0)
	{
		return;
	}

	system("cls");
	sort(infoVector.begin(), infoVector.end(), isletterGreater);
	showInfoArray();
	


}

bool isletterGreater(Info first, Info second)
{
	return first.country < second.country;
}


void individualTask()
{
	if (backMunu() == 0)
	{
		return;
	}
	system("cls");
	cout << "\t\t __________________________________________________________________________________________________________" << endl;
	cout << "\t\t|" << "\t\t\t\t� � � � � � � � � � � � � �   � � � � � � �" << "\t\t\t\t   |" << endl;
	cout << "\t\t|__________________________________________________________________________________________________________|" << endl;
	
	int selectedValue;
	string selectedTour;
	vector<Info> tempInfoVector;
	tempInfoVector = infoVector;
	sort(tempInfoVector.begin(), tempInfoVector.end(), isNumberGreater);

	cout << "\n\t\t������� ��� �������: ";
	selectedTour = getTour();
	cout << "\n\t\t������� ������������ ���������: ";
	selectedValue = getValue(0, 1000000);


	cout << "\n\t _______________________________________________________________________________________________________________________" << endl;
	cout << "\t| � |\t��� ����   | " << "\t   ������       | " << "    ��� �����������    | " << "   ���-�� ����    | " << "  ��� �������  |" << "       ����      | " << endl;
	cout << "\t|___|______________|____________________|________________________|___________________|________________|_________________|" << endl;
	for (int i = 0; i < infoVector.size(); i++)
	{
		if (selectedTour == tempInfoVector[i].typeOfTour && tempInfoVector[i].value <= selectedValue)
		{
			cout << "\t| " << i + 1 << " |\t";
			cout << setw(11) << left;
			cout << tempInfoVector[i].typeOfTour << "|\t   ";
			cout << setw(10) << left;
			cout << tempInfoVector[i].country << "\t|\t  ";
			cout << setw(13) << left;
			cout << tempInfoVector[i].transport << "\t |\t  ";
			cout << setw(6) << left;
			cout << tempInfoVector[i].daysAmount << "     |\t ";
			cout << setw(8) << left;
			cout << tempInfoVector[i].food << "     |\t      ";
			cout << setw(8) << left;
			cout << tempInfoVector[i].value << "  |" << endl;
			cout << setw(8) << left
				<< "\t|___|______________|____________________|________________________|___________________|________________|_________________|\n";
		}
	}

	system("pause");
	



}

//  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ���� ~~~~~~~~~~~~~~~~~~~~~~~~~~~



void ProgramRealization(int user_role)
{
	bool exit_flag = true;

	while (exit_flag)
	{
		switch (user_role)
		{
		case ADMIN:

			switch (AdminMenu())
			{
			case 27: exit_flag = false; break;
			case '1': AccountsMenu(); break;
			case '2': FileMenu(); break;
			case '3': DataMenu(); break;
			case '4': ChangeUser(user_role); break;
			}break;

			//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		case USER:

			switch (UserMenu())
			{
			case 27: exit_flag = false; break;
			case '1': showInfoArray(); break;
			case '2': proccesMenu(); break;
			case '3': ChangeUser(user_role); break;
			}break;
		}

	}


}

int AdminMenu()
{
	system("cls");
	cout << "  \t\t __________________________________________________________________________________________________________" << endl;
	cout << "  \t\t|" << "\t\t\t\t\t  � � � � � � �   � � � �" << "\t\t\t\t\t   |" << endl;
	cout << "  \t\t|__________________________________________________________________________________________________________|" << endl;
	cout << endl;
	
	cout << "  \t\t _______________________________________________________________________________" << endl;
	cout << "  \t\t| 1 |   � � � � � �   �   � � � � � � � �   � � � � � � � �" << "\t\t\t|" << endl;
	cout << "  \t\t|___|___________________________________________________________________________|" << endl;
	cout << "  \t\t _______________________________________________________________________________" << endl;
	cout << "  \t\t| 2 |   � � � � � �   �   � � � � � � �" << "\t\t\t\t\t\t|" << endl;
	cout << "  \t\t|___|___________________________________________________________________________|" << endl;
	cout << "  \t\t _______________________________________________________________________________" << endl;
	cout << "  \t\t| 3 |   � � � � � �   �   � � � � � � �" << "\t\t\t\t\t\t|" << endl;
	cout << "  \t\t|___|___________________________________________________________________________|" << endl;
	cout << "  \t\t _______________________________________________________________________________" << endl;
	cout << "  \t\t| 4 |   C � � � � � �   � � � � � � � � � � � �" << "\t\t\t\t\t|" << endl;
	cout << "  \t\t|___|___________________________________________________________________________|" << endl;
	cout << "\t\t _________________________________" << endl;
	cout << "\t\t| ESC |" << "         � � � � � \t  |" << endl;
	cout << "\t\t|_____|___________________________|" << endl;
	

	

	int choise = _getch();
	return choise;
}






void AccountsMenu()
{
	system("cls");
	bool exit_flag = true;
	int choice;
    
	while (exit_flag)
	{
		system("cls");
		cout << "  \t\t __________________________________________________________________________________________________________" << endl;
		cout << "  \t\t| \t\t\t      � � � � � �   �   � � � � � � � �   � � � � � � � �" << "\t\t\t   |" << endl;
		cout << "  \t\t|__________________________________________________________________________________________________________|" << endl;
		cout << "  \t\t _________________________________________________________________________________" << endl;
		cout << "  \t\t|  1  |   � � � � � � � � � � �   � � � � � � �   � � � � � �" << "\t\t\t  |" << endl;
		cout << "  \t\t|_____|___________________________________________________________________________|" << endl;
		cout << "  \t\t _________________________________________________________________________________" << endl;
		cout << "  \t\t|  2  |   � � � � � � � �   � � � � � � �   � � � � � �" << "\t\t\t\t  |" << endl;
		cout << "  \t\t|_____|___________________________________________________________________________|" << endl;
		cout << "  \t\t _________________________________________________________________________________" << endl;
		cout << "  \t\t|  3  |   � � � � � � � � � � � � �   � � � � � � �   � � � � � �" << "\t\t  |" << endl;
		cout << "  \t\t|_____|___________________________________________________________________________|" << endl;
		cout << "  \t\t _________________________________________________________________________________" << endl;
		cout << "  \t\t|  4  |   � � � � � � �   � � � � � � �   � � � � � �" << "\t\t\t\t  |" << endl;
		cout << "  \t\t|_____|___________________________________________________________________________|" << endl;
		cout << "\t\t _________________________________" << endl;
		cout << "\t\t| ESC |" <<"         � � � � � \t  |"<< endl;
		cout << "\t\t|_____|___________________________|" << endl;
		
	

		choice = _getch();

		switch (choice)
		{
		case '1': showUserArray(); break;
		case '2': addUserinArray(); break;
		case '3': updateUserArray(); break;
		case '4': deleteUserArray(); break;
		case 27: exit_flag = false; break;
		}


	}

	
}

void FileMenu()
{
	system("cls");
	bool exit_flag = true;
	int choice;

	while (exit_flag)
	{
		system("cls");
		cout << "  \t\t __________________________________________________________________________________________________________" << endl;
		cout << "  \t\t| \t\t\t\t      � � � � � �   �   � � � � � � �" << "\t\t\t\t\t   |" << endl;
		cout << "  \t\t|__________________________________________________________________________________________________________|" << endl;
		cout << "  \t\t _________________________________________________________________________________" << endl;
		cout << "  \t\t|  1  |   � � � � � � �   � � � � " << "\t\t\t\t\t\t  |" << endl;
		cout << "  \t\t|_____|___________________________________________________________________________|" << endl;
		cout << "  \t\t _________________________________________________________________________________" << endl;
		cout << "  \t\t|  2  |   � � � � � � �   � � � �" << "\t\t\t\t\t\t  |" << endl;
		cout << "  \t\t|_____|___________________________________________________________________________|" << endl;
		cout << "  \t\t _________________________________________________________________________________" << endl;
		cout << "  \t\t|  3  |   � � � � � � �   � � � �" << "\t\t\t\t\t\t  |" << endl;
		cout << "  \t\t|_____|___________________________________________________________________________|" << endl;
		cout << "\t\t _________________________________" << endl;
		cout << "\t\t| ESC |" << "         � � � � � \t  |" << endl;
		cout << "\t\t|_____|___________________________|" << endl;

		choice = _getch();

		switch (choice)
		{
		case '1': createFile(); break;
		case '2': openFile(); break;
		case '3': deleteFile(); break;
		case 27: exit_flag = false; break;
		}

	}
}




void DataMenu()
{
	system("cls");
	bool exit_flag = true;
	int choice;

	while (exit_flag)
	{
		system("cls");
		cout << "  \t\t __________________________________________________________________________________________________________" << endl;
		cout << "  \t\t| \t\t\t\t      � � � � � �   �   � � � � � � �" << "\t\t\t\t\t   |" << endl;
		cout << "  \t\t|__________________________________________________________________________________________________________|" << endl;
		cout << "  \t\t _________________________________________________________________________________" << endl;
		cout << "  \t\t|  1  |   � � � � �   � � � � � � � � � � � � � � " << "\t\t\t\t  |" << endl;
		cout << "  \t\t|_____|___________________________________________________________________________|" << endl;
		cout << "  \t\t _________________________________________________________________________________" << endl;
		cout << "  \t\t|  2  |   � � � � �   � � � � � � � � �" << "\t\t\t\t\t\t  |" << endl;
		cout << "  \t\t|_____|___________________________________________________________________________|" << endl;
		cout << "\t\t _________________________________" << endl;
		cout << "\t\t| ESC |" << "         � � � � � \t  |" << endl;
		cout << "\t\t|_____|___________________________|" << endl;

		choice = _getch();

		switch (choice)
		{
		case '1': EditMenu(); break;
		case '2': proccesMenu(); break;
	
	              

		
		case 27: exit_flag = false; break;
		}


	}
}

void EditMenu()
{
	int choice;
	bool exit_flag = true;
	while (exit_flag)
	{
		system("cls");
		cout << "  \t\t __________________________________________________________________________________________________________" << endl;
		cout << "  \t\t| \t\t\t\t   � � � � �   � � � � � � � � � � � � � �" << "\t\t\t\t   |" << endl;
		cout << "  \t\t|__________________________________________________________________________________________________________|" << endl;
		cout << "  \t\t _________________________________________________________________________________" << endl;
		cout << "  \t\t|  1  |   � � � � � � � � � � �   � � � � � �" << "\t\t\t\t\t  |" << endl;
		cout << "  \t\t|_____|___________________________________________________________________________|" << endl;
		cout << "  \t\t _________________________________________________________________________________" << endl;
		cout << "  \t\t|  2  |   � � � � � � � �   � � � � � �" << "\t\t\t\t\t\t  |" << endl;
		cout << "  \t\t|_____|___________________________________________________________________________|" << endl;
		cout << "  \t\t _________________________________________________________________________________" << endl;
		cout << "  \t\t|  3  |   � � � � � � � � � � � � �   � � � � � �" << "\t\t\t\t  |" << endl;
		cout << "  \t\t|_____|___________________________________________________________________________|" << endl;
		cout << "  \t\t _________________________________________________________________________________" << endl;
		cout << "  \t\t|  4  |   � � � � � � �   � � � � � �" << "\t\t\t\t\t\t  |" << endl;
		cout << "  \t\t|_____|___________________________________________________________________________|" << endl;
		cout << "\t\t _________________________________" << endl;
		cout << "\t\t| ESC |" << "         � � � � � \t  |" << endl;
		cout << "\t\t|_____|___________________________|" << endl;

		choice = _getch();

		switch (choice)
		{
		case '1': showInfoArray(); break;
		case '2': addInfoinArray(); break;
		case '3': updateInfoArray(); break;
		case '4': deleteInfoArray(); break;
		case 27: exit_flag = false; break;

		}
	}
}


void proccesMenu()
{

	int choice;
	bool exit_flag = true;

	while (exit_flag)
	{

		system("cls");
		cout << "  \t\t __________________________________________________________________________________________________________" << endl;
		cout << "  \t\t| \t\t\t\t\t   � � � � �   � � � � � � � � �" << "\t\t\t\t   |" << endl;
		cout << "  \t\t|__________________________________________________________________________________________________________|" << endl;
		cout << "  \t\t _________________________________________________________________________________" << endl;
		cout << "  \t\t|  1  |   � � � � �   � � � � � �" << "\t\t\t\t\t\t  |" << endl;
		cout << "  \t\t|_____|___________________________________________________________________________|" << endl;
		cout << "  \t\t _________________________________________________________________________________" << endl;
		cout << "  \t\t|  2  |   � � � � � � � � � �   � � � � � �" << "\t\t\t\t\t  |" << endl;
		cout << "  \t\t|_____|___________________________________________________________________________|" << endl;
		cout << "  \t\t _________________________________________________________________________________" << endl;
		cout << "  \t\t|  3  |   � � � � � � � � � �   � � � � � � �" << "\t\t\t\t\t  |" << endl;
		cout << "  \t\t|_____|___________________________________________________________________________|" << endl;
		cout << "\t\t _________________________________" << endl;
		cout << "\t\t| ESC |" << "         � � � � � \t  |" << endl;
		cout << "\t\t|_____|___________________________|" << endl;

		choice = _getch();

		switch (choice)
		{
		case '1': dataSearchMenu(); break;
		case '2': dataSortingMenu(); break;
		case '3': individualTask(); break;
		case 27: exit_flag = false; break; 

		}


		




	}
}



void dataSearchMenu()
{

	int choice;
	bool exit_flag = true;
	while (exit_flag)
	{
		system("cls");
		cout << "  \t\t __________________________________________________________________________________________________________" << endl;
		cout << "  \t\t| \t\t\t\t\t   � � � � �   � � � � � �   � � � � � �" << "\t\t\t   |" << endl;
		cout << "  \t\t|__________________________________________________________________________________________________________|" << endl;
		cout << "  \t\t _________________________________________________________________________________" << endl;
		cout << "  \t\t|  1  |   � � � � �   � �   � � � � � �" << "\t\t\t\t\t\t  |" << endl;
		cout << "  \t\t|_____|___________________________________________________________________________|" << endl;
		cout << "  \t\t _________________________________________________________________________________" << endl;
		cout << "  \t\t|  2  |   � � � � �   � �   � � � �   � � � � � � �" << "\t\t\t\t  |" << endl;
		cout << "  \t\t|_____|___________________________________________________________________________|" << endl;
		cout << "  \t\t _________________________________________________________________________________" << endl;
		cout << "  \t\t|  3  |   � � � � �   � �   � � � �   � � � � � � � � � �" << "\t\t\t  |" << endl;
		cout << "  \t\t|_____|___________________________________________________________________________|" << endl;
		cout << "\t\t _________________________________" << endl;
		cout << "\t\t| ESC |" << "         � � � � � \t  |" << endl;
		cout << "\t\t|_____|___________________________|" << endl;

		choice = _getch();

		switch (choice)
		{
		case '1': searchByCountry(); break;
		case '2': searchByTypeOfTour(); break;
		case '3': searchByTypeOfTransport(); break;
		case 27: exit_flag = false; break;
		}
	}

}


void dataSortingMenu()
{
	int choice;
	bool exit_flag = true;
	while (exit_flag)
	{
		system("cls");
		cout << "  \t\t __________________________________________________________________________________________________________" << endl;
		cout << "  \t\t| \t\t\t\t\t   � � � � �   C � � � � � � � � �    \t\t\t\t   |" << endl;
		cout << "  \t\t|__________________________________________________________________________________________________________|" << endl;
		cout << "  \t\t _________________________________________________________________________________" << endl;
		cout << "  \t\t|  1  |   C � � � � � � � � �   � �   � � � � � � �  ( � � � � � � � )"  "\t\t  |" << endl;
		cout << "  \t\t|_____|___________________________________________________________________________|" << endl;
		cout << "  \t\t _________________________________________________________________________________" << endl;
		cout << "  \t\t|  2  |   C � � � � � � � � �   � �   � � � �" << "\t\t\t\t\t  |" << endl;
		cout << "  \t\t|_____|___________________________________________________________________________|" << endl;
		cout << "  \t\t _________________________________________________________________________________" << endl;
		cout << "  \t\t|  3  |   C � � � � � � � � �   � �   � � � - ��   � � � �" << "\t\t\t  |" << endl;
		cout << "  \t\t|_____|___________________________________________________________________________|" << endl;
		cout << "\t\t _________________________________" << endl;
		cout << "\t\t| ESC |" << "         � � � � � \t  |" << endl;
		cout << "\t\t|_____|___________________________|" << endl;

		choice = _getch();

		switch (choice)
		{
		case '1': sortByCountryAlphabet(); break;
		case '2': sortByPrice(); break;
		case '3': sortByQuantityOfDay(); break;
		case 27: exit_flag = false; break;
		}
	}
}


int UserMenu()
{

	system("cls");
	cout << "  \t\t __________________________________________________________________________________________________________" << endl;
	cout << "  \t\t|" << "\t\t\t\t\t  � � � � � � �   � � � �" << "\t\t\t\t\t   |" << endl;
	cout << "  \t\t|__________________________________________________________________________________________________________|" << endl;
	cout << endl;

	cout << "  \t\t _______________________________________________________________________________" << endl;
	cout << "  \t\t| 1 |   � � � � � � � � � � �   � � � � � � " << "\t\t\t\t\t|" << endl;
	cout << "  \t\t|___|___________________________________________________________________________|" << endl;
	cout << "  \t\t _______________________________________________________________________________" << endl;
	cout << "  \t\t| 2 |   � � � � � �   �   � � � � � � �" << "\t\t\t\t\t\t|" << endl;
	cout << "  \t\t|___|___________________________________________________________________________|" << endl;
	cout << "  \t\t _______________________________________________________________________________" << endl;
	cout << "  \t\t| 3 |   C � � � � � �   � � � � � � � � � � � �" << "\t\t\t\t\t|" << endl;
	cout << "  \t\t|___|___________________________________________________________________________|" << endl;
	cout << "\t\t _________________________________" << endl;
	cout << "\t\t| ESC |" << "         � � � � � \t  |" << endl;
	cout << "\t\t|_____|___________________________|" << endl;


	int choise = _getch();
	return choise;

}




int ConfirmAction()
{
	system("cls");
	int choice;

	cout << "\n\n\n\n\n\n\n";
	cout << "\t\t\t\t _________________________________________________________________" << endl;
	cout << "\t\t\t\t|                                                                 |" << endl;
	cout << "\t\t\t\t|\t      � � � � � � � � � � �   � � � � � � � �  \t\t  | " << endl;
	cout << "\t\t\t\t|                                                                 |" << endl;
	cout << "\t\t\t\t|_________________________________________________________________|" << endl;
	cout << "\t\t\t\t|                                |                                |" << endl;
	cout << "\t\t\t\t|                                |                                |" << endl;
	cout << "\t\t\t\t|     ����������� - ENTER        |          ������ - ESC          |" << endl;
	cout << "\t\t\t\t|                                |                                |" << endl;
	cout << "\t\t\t\t|________________________________|________________________________|" << endl;

	choice = _getch();

	return choice;

}




void CompletedAction()
{
	system("cls");
	cout << "\n\n\n\n\n\n\n";
	cout << "\t\t\t\t\t _______________________________________________" << endl;
	cout << "\t\t\t\t\t|                                               |" << endl;
	cout << "\t\t\t\t\t|              � � � � � � � � � !              |" << endl;
	cout << "\t\t\t\t\t|                                               |" << endl;
	cout << "\t\t\t\t\t|_______________________________________________|\n\n" << endl;

	system("pause");
}
